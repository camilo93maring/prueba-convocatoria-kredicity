<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carros extends Model
{
    protected $guarded = [];

    protected $primaryKey = 'id_car';
}
