<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{	
	// Read
    public function listUsers(Request $request){
    	// Funcion que lista usuarios

    	/*$userName = User::all();
    	// dd($userName);
    	
    	return view('userList', [
    		'userName' => $userName
    	]);*/
        
        
        $keyword = $request->get('search');
        $users = User::where('name', 'LIKE', '%' . $keyword . '%')->paginate(5);

        if ($request->ajax()) {
              return view('partial', compact('users', 'keyword') )->render();  
        }
        return view('userList', compact('users', 'keyword'));

    }

    /* Create La funcion crear ya está creada en el AUTH*/
    public function createUser(Request $request){
		/*$user = User::create([

		]);*/
		$message = [];

		$createUser = User::updateOrInsert(
			            [
			            	'email' => $request->input('email')
			            ],
			            [
			            'name' => $request->input('name'),
			            'password' => Hash::make($request->input('password')),
			            'ipClient' => request()->ip()
			          	]
			          	);
		// dd($createUser);
		if ($createUser) {
			array_push($message, 'Usuario creado');
		}else{
			array_push($message, 'No se creó el usuario');
		}

		/*return $message;*/
        return redirect()->route('userList');

    }

    // delete
    public function deleteUser(Request $request){
    	// Funcion que borra un usuario

    	$message = [];
    	$deleteUser = User::where('id', $request->input('idUser'))->delete();
    	// dd($deleteUser);

    	if ($deleteUser) {
    		array_push($message, 'se borro el usuario');
    	}else{
    		array_push($message, 'No se borro el usuario');
    	}

    	/*return $message;*/
        return redirect()->route('userList');
    }

    // update
    public function editUser(Request $request){
    	
    	// editar usuarios
    	$editUser = User::where('id',$request->input('idUser'))
    				->update([
    					'name' => $request->input('name'),
    					'email' => $request->input('email'),
    					'password' => $request->input('password'),
    				]);

        return redirect()->route('userList');
    }

    // Busqueda de usuarios
    public function searchUsers(Request $request){

        /*$keyword = $request->get('search');
        $users = User::where('name', 'LIKE', '%' . $keyword . '%')->paginate(5);

        if ($request->ajax()) {
              return view('partial', compact('users', 'keyword') )->render();  
        }
        return view('admin.user_rapid.index', compact('users', 'keyword'));*/
    }
}
