<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    // public function reset(Request $request)
    // {   
    //     /*return "Este es el reset";*/
    //     $request->validate($this->rules(), $this->validationErrorMessages());
    //     dd($request);
    //     // Guardar nuevas credenciales
    //     User::where('email',$request->input('emailReset'))->update([
    //         'password' => $request->input('passwordReset'),
    //     ]);
    // }
}
