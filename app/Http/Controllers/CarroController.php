<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carros;

class CarroController extends Controller
{
	// Create crear carros
    public function createCar(Request $request)
    {
    	$message = [];

    	$createCar = Carros::updateOrInsert(
			            [
			            	'id_car' => $request->input('id_car')
			            ],
			            [
			            'brand' => $request->input('brand'),
			            'model' => $request->input('model')
			          	]
			          	);

    	if ($createCar) {
    		array_push($message, 'Se creó el carro');
    	}else{
    		array_push($message, 'No se creó el carro');
    	}

    	/*return $message;*/
    	return redirect()->route('carList');
    }

    // Read listar los carros
    public function listCar(){

    	$listCar = Carros::all();

    	return view('carsList', [
    		'listCar' => $listCar
    	]);
    }

    // Update editar carro
    public function editCar(Request $request){

    	$message = [];

    	$editCar = Carros::where('id_car', $request->input('id_car'))->update([
    						'brand' =>	$request->input('brand'),
    						'model' =>	$request->input('model')
    				]);

    	if ($editCar) {
    		array_push($message, 'Se edito el carro');
    	}else{
    		array_push($message, 'No se edito el carro');
    	}

    /*return $message;*/
    return redirect()->route('carList');

    }

    // Delete borrar carros
    public function deleteCar(Request $request){

    	$message = [];

    	$deleteCar = Carros::where('id_car', $request->input('id_car'))->delete();

    	/*dd($deleteCar);*/

    	if ($deleteCar) {
    		array_push($message, 'Se eliminó le carro');
    	}else{
    		array_push($message, 'No se eliminó le carro');
    	}

    	/*return $message;*/
    	return redirect()->route('carList');
    }
    
}
