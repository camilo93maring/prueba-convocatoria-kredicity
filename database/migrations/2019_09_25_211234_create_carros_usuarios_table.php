<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrosUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carros_usuarios', function (Blueprint $table) {
            $table->increments('id_car_user');
            $table->integer('id_car');
            $table->integer('id_user');
            $table->string('kilometers');
            $table->foreign('id_car')->references('id_car')->on('carros');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carros_usuarios');
    }
}
