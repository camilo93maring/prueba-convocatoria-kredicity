@extends('layouts.app')
{{-- {{ $userName }} --}}

@section('content')
	{{-- <div class="row"> --}}
		@if (Route::has('login'))
			@auth
            {{-- <form class="container_form">
                <div class="input-group mb-3">  
                    @csrf
                    <input type="text" class="form-control" name="search" id="search" value="{{ $keyword }}" placeholder="Search..">
                    <div class="input-group-append">
                        <button class="btn btn-info" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form> --}}
				@foreach ($listCar as $valor)
					<form action="/tipo/basic">
					<li>	{{ $valor->brand }}</li>
					{{-- <button type="submit">editar</button> --}}
					</form>

					<form action="{{ route('carDelete') }}">
						<input type="hidden" name="id_car" value="{{ $valor->id_car }}">
						<button type="submit">delete</button>
					</form>

					<form action="{{ route('carEdit') }}">
						<input type="hidden" name="id_car" value="{{ $valor->id_car }}">
						<label>brand</label>
						<input type="text" name="brand">
						<label>modelo</label>
						<input type="text" name="model">
						
						<button type="submit">edit</button>
					</form>

					{{-- <form action="{{ route('userCreate') }}">
						<input type="hidden" name="idUser" value="{{ $valor->id }}">
						<button type="submit">create</button>
					</form> --}}

					
				@endforeach

				{{-- Formulario create --}}

					<form method="POST" action="{{ route('carCreate') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="brand" class="col-md-4 col-form-label text-md-right">{{ __('Brand') }}</label>

                            <div class="col-md-6">
                                <input id="brand" type="text" class="form-control{{ $errors->has('brand') ? ' is-invalid' : '' }}" name="brand" value="{{ old('brand') }}" required autofocus>

                                @if ($errors->has('brand'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('brand') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="text" class="col-md-4 col-form-label text-md-right">{{ __('model') }}</label>

                            <div class="col-md-6">
                                <input id="model" type="text" class="form-control{{ $errors->has('model') ? ' is-invalid' : '' }}" name="model" value="{{ old('model') }}" required>

                                @if ($errors->has('model'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('model') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create Car') }}
                                </button>
                            </div>
                        </div>
                    </form>


			@endauth
		@endif
		@include('sweet::alert')
	{{-- </div> --}}
<script> 
    swal({
        "timer":1800,
        "title":"Confirmar",
        "text":"Borrar usuario?",
        "showConfirmButton":false
    });
</script>
@endsection