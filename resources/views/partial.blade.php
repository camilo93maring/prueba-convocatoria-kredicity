@if (count($users) > 0)
@foreach($users as $user)
<main role="main" class="container">
   <div>
       <h3 class="pagination">
           {{-- <a href="{{ action('adminController@listUsers', [$user->id]) }}">{{$user->name }}</a> --}}
           <a href="{{ route('userList') }}">{{$user->name }}</a>
           {{-- <p>{{$user->name }}</p> --}}
       </h3>

       <form action="/tipo/basic">
		<li>	{{ $user->name }}</li>
		{{-- <button type="submit">editar</button> --}}
		</form>

		<form action="{{ route('userDelete') }}">
			<input type="hidden" name="idUser" value="{{ $user->id }}">
			<button type="submit">delete</button>
		</form>

		<form action="{{ route('userEdit') }}">
			<input type="hidden" name="idUser" value="{{ $user->id }}">
			<label>nombre</label>
			<input type="text" name="name" placeholder="{{ $user->name }}">
			<label>Email</label>
			<input type="email" name="email" placeholder="{{ $user->email }}">
			<label>password</label>
			<input type="password" name="password">
			<button type="submit">edit</button>
		</form>
   </div>
  </main>
@endforeach
{{-- Formulario create --}}

					<form method="POST" action="{{ route('userCreate') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form> 
@else
No hay resultados
@endif
</div>
{{ $users->links() }}