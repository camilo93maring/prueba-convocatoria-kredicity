@extends('layouts.app')
{{-- {{ $userName }} --}}

@section('content')
	{{-- <div class="row"> --}}
		@if (Route::has('login'))
			@auth

            <form class="container_form">
                <div class="input-group mb-3">  
                    @csrf
                    <input type="text" class="form-control" name="search" id="search" value="{{ $keyword }}" placeholder="Search..">
                    <div class="input-group-append">
                        <button class="btn btn-info" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>

            <div class="users" class="starter-template">
                @extends('partial')
            </div>

				{{-- @foreach ($userName as $valor)
					<form action="/tipo/basic">
					<li>	{{ $valor->name }}</li>
					<button type="submit">editar</button>
					</form>

					<form action="{{ route('userDelete') }}">
						<input type="hidden" name="idUser" value="{{ $valor->id }}">
						<button type="submit">delete</button>
					</form>

					<form action="{{ route('userEdit') }}">
						<input type="hidden" name="idUser" value="{{ $valor->id }}">
						<label>nombre</label>
						<input type="text" name="name">
						<label>Email</label>
						<input type="email" name="email">
						<label>password</label>
						<input type="password" name="password">
						<button type="submit">edit</button>
					</form> --}}

					{{-- <form action="{{ route('userCreate') }}">
						<input type="hidden" name="idUser" value="{{ $valor->id }}">
						<button type="submit">create</button>
					</form> --}}

					
				{{-- @endforeach --}}

				{{-- Formulario create --}}

					{{-- <form method="POST" action="{{ route('userCreate') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form> --}}


			@endauth
		@endif
		@include('sweet::alert')
	{{-- </div> --}}
    @section('scripts')

    <script type="text/javascript">

        $(function() {
            $('body').on('click', '.pagination a', function(e) {
                e.preventDefault();

                var url = $(this).attr('href');
                getUsers(url);
                window.history.pushState("", "", url);
            });

            $('body').on('keyup', '#search', function(e) {
                e.preventDefault();

                var url = $(this).attr('href');
                getUsers(url);
                window.history.pushState("", "", url);
            });

            function getUsers(url) {
                $.ajax({
                    url : url,
                    type : "get",
                    data:{
                        search: $('#search').val()
                    },
                }).done(function (data) {
                    $('.users').html(data);
                }).fail(function () {
                    alert('Users could not be loaded.');
                });
            }

            function myFunction(){
            }
        });
    </script>

    {{-- <script> 
        swal({
            "timer":1800,
            "title":"Confirmar",
            "text":"Borrar usuario?",
            "showConfirmButton":false
        });
    </script> --}}
    @endsection
@endsection