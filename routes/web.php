<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('tipo/{type}', 'SweetController@notification');

Route::get('/admin/inicio', function () {
    return view('admin');
});

// rutas de AUTH laravel doc
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// rutas para el crud de usuarios

Route::get('/users/list', 'AdminController@listUsers')->name('userList');

Route::get('/users/delete', 'AdminController@deleteUser')->name('userDelete');

Route::get('/users/edit', 'AdminController@editUser')->name('userEdit');

Route::post('/users/create', 'AdminController@createUser')->name('userCreate');

// rutas para crud de carros

Route::get('/cars/list', 'CarroController@listCar')->name('carList');

Route::get('/cars/delete', 'CarroController@deleteCar')->name('carDelete');

Route::get('/cars/edit', 'CarroController@editCar')->name('carEdit');

Route::post('/cars/create', 'CarroController@createCar')->name('carCreate');